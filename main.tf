variable "gcs_keys" {
  type = string
}

variable "project_id" {
  type = string
}

variable "location" {
  type = string
}

provider "google" {
  credentials = file(var.gcs_keys)
  project     = var.project_id
}

# Create new storage bucket in GCE
resource "google_storage_bucket" "static" {
  name          = "futurae_assignment_bucket"
  location      = var.location
  storage_class = "STANDARD"

  uniform_bucket_level_access = true
}
