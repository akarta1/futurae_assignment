# GCS Enumerator

A simple python 3 program that enumerates the contents of a GCS bucket and creates an HTTP endpoint


## Installation

You should have a working kubernetes k3d cluster running locally or load the docker image accordingly to your local k8s installation

All commands run from the project root directory

Provide your GCS credentials, your project ID and location as environmental variables using the names `TF_VAR_gcs_keys`, `TF_VAR_project_id` and `TF_VAR_location`

e.g.
```bash
export TF_VAR_gcs_keys=~/.config/gcloud/application_default_credentials.json
export TF_VAR_project_id=futurae-404406
export TF_VAR_location=europe-west1
```

Create the bucket using terraform.

```bash
terraform apply
```

Create the docker image and import it to k3d
```bash
docker build -t enumerate-gcs .
k3d image import enumerate-gcs
```

Copy the GCS credentials to helm directory
```bash
cp $TF_VAR_gcs_keys enumerate-gcs/gcs_keys.json
```

Install the helm chart
```bash
helm install --set projectID=$TF_VAR_project_id enumerate-gcs enumerate-gcs
```

## Test

To test just make a GET request on port `8001`. You can get the external service ip using `kubectl get svc enumerate-gcs -o jsonpath='{.status.loadBalancer.ingress[0].ip}'`

e.g
```bash
  curl -X GET $(kubectl get svc enumerate-gcs -o jsonpath='{.status.loadBalancer.ingress[0].ip}'):8001
```
