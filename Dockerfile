# Use a base image with your desired programming language and runtime
FROM python:3.11

# Copy your program and any necessary files to the container
COPY enumerate_gcs.py /app/enumerate_gcs.py

# Set the GOOGLE_APPLICATION_CREDENTIALS environment variable
ENV GOOGLE_APPLICATION_CREDENTIALS /app_secrets/gcs_keys.json

# Install any necessary dependencies
RUN pip install google-cloud-storage

# Specify the command to run your program
CMD ["python3", "/app/enumerate_gcs.py"]
