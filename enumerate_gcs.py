#!/usr/bin/env python

import http.server
import os
import json
import socketserver

from google.cloud import storage

PROJECT_ID = os.getenv("PROJECT_ID")
PORT = 8001


class Handler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/json")
        self.end_headers()

        output_data = list_bucket_objects("futurae_assignment_bucket")
        self.wfile.write(output_data.encode("utf-8"))


def list_bucket_objects(bucket_name):
    storage_client = storage.Client(project=PROJECT_ID)
    bucket = storage_client.get_bucket(bucket_name)
    objects = list(bucket.list_blobs())
    bucket_contents = [obj.name for obj in objects]

    return json.dumps(bucket_contents)


if __name__ == "__main__":
    list_bucket_objects("futurae_assignment_bucket")

    with socketserver.TCPServer(("", PORT), Handler) as httpd:
        print(f"Starting http://0.0.0.0:{PORT}")
        httpd.serve_forever()
